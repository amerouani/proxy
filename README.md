# Proxy

Projet ayant pour but de minifier les actions utilisateurs entre redmine teamwork et gitlab.

## Configuration du projet

### Installer les dépendances

```sh
npm install
```

### Tester le projet

```sh
npm run dev
```

### Produire le projet

```sh
npm run dev
```
