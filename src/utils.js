import Configuration from "./config";
import axios from "axios";

export const ca_tag = "[CA]";
export const in_tag = "[IN]";

export const subject_template = (tracker, id, subject) =>
	`${
		tracker.charAt(0).toUpperCase() + tracker.substring(1)
	} #${id}: ${subject}`;

export const description_template = (description) =>
	`======= Description client =======\n\n${description}\n\n======= Description tâche =======\n`;

export const redmine_priorities = [
	{
		label: "Basse",
		value: "[L]",
	},
	{
		label: "Normal",
		value: "[M]",
	},
	{
		label: "Haute",
		value: "[H]",
	},
	{
		label: "Urgent",
		value: "[E]",
	},
	{
		label: "Immediat",
		value: "[I]",
	},
];
export const toOptions = (val, label, value) => {
	let arr = [];
	val.forEach((element) => {
		arr.push({
			label: element[label],
			value: element[value],
		});
	});
	return arr;
};

export const formatDate = (date) => {
	return `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${(
		"0" + date.getDate()
	).slice(-2)}`;
};

export const formatDateTime = (dateTime) => {
	return `${("0" + dateTime.getHours()).slice(-2)}:${(
		"0" + dateTime.getMinutes()
	).slice(-2)}`;
};
export class Mapping {
	constructor(core) {
		this.core = core;
		this.tags = Configuration.teamwork_tags;
	}

	tw_get_date() {
		let today = new Date();
		return `${today.getFullYear()}${("0" + (today.getMonth() + 1)).slice(-2)}${(
			"0" + today.getDate()
		).slice(-2)}`;
	}
	async fetchAllRedmineUsers() {
		try {
			let res = await axios({
				method: "GET",
				url: `${Configuration.redmine_api_url}/users.json`,
				params: {
					key: "058cccefacb6dbfa9fd851b8ee25fd3f0143dacd",
				},
			});
			let count = res.data.total_count;
			let maxPageSize = 100;
			let iter = Math.ceil(count / maxPageSize);
			let users = [];
			for (let i = 0; i < iter; i++) {
				let result = await axios({
					method: "GET",
					url: `${
						Configuration.redmine_api_url
					}/users.json?limit=${maxPageSize}&offset=${maxPageSize * i}`,
					params: {
						key: Configuration.redmine_token,
					},
				});
				users = users.concat(result.data.users);
			}
			return users;
		} catch (err) {
			this.error = {
				title: "Impossible de récupérer les utilisateurs teamwork .",
				content: "Le token est-il bien spécifié ?",
			};
			console.warn(err);
			return -1;
		}
	}
	tw_get_priority(priority) {
		let output = "";
		switch (priority.name.slice(-3)) {
			case "[L]": {
				output = "low";
				break;
			}
			case "[M]": {
				output = "medium";
				break;
			}
			default: {
				output = "high";
				break;
			}
		}
		return output;
	}
	rd_get_priority(value) {
		switch (value) {
			case "low":
				return 1;
			case "medium":
				return 2;
			case "high":
				return 3;
		}
	}

	tw_get_start_date() {
		if (this.core.start_date == undefined || null) {
			return this.tw_get_date();
		} else {
			return this.core.start_date.split("T")[0].replaceAll("-", "");
		}
	}

	tw_get_estimated_mins() {
		return this.core.total_estimated_hours * 60;
	}
	rd_get_estimated_hours() {
		return this.core["estimated-minutes"] / 60;
	}

	tw_get_due_date() {
		if (this.core.due_date == undefined || null) {
			return null;
		} else {
			return this.core.due_date.replaceAll("-", "");
		}
	}

	rd_get_assigned(id) {
		try {
			this.fetchAllRedmineUsers().then((users) => {
				let assigned_user = undefined;
				users.forEach((el) => {
					el["custom_fields"].forEach((element) => {
						if (element.name == Configuration.user_foreign_key) {
							if (element.value == id) {
								assigned_user = el;
							}
						}
					});
				});
				if (assigned_user !== undefined) {
					return assigned_user.id;
				} else {
					return null;
				}
			});
		} catch (error) {
			console.warn(error);
		}
	}
	async format(type) {
		if (type == "toTeamwork") {
			let tags = [];
			tags.push(this.tags.find((x) => x.name == "REDMINE")); // tag REDMINE
			if (
				this.core.custom_fields.some(
					(x) => x.name == "SERVICE DEMANDEUR KOHLER EMEA"
				)
			) {
				tags.push(
					this.tags.find(
						(x) =>
							x.name ==
							this.core.custom_fields.find(
								(y) => y.name == "SERVICE DEMANDEUR KOHLER EMEA"
							).value
					)
				);
			}
			return {
				"todo-item": {
					"use-defaults": false,
					completed: false,
					content: this.core.subject,
					"creator-id": this.core.tw_author,
					notify: false,
					"responsible-party-id": this.core.assigned_to.tw_id,
					"start-date": this.tw_get_start_date(),
					"due-date": this.tw_get_due_date(),
					description: this.core.description,
					priority: this.tw_get_priority(this.core.priority),
					"everyone-must-do": false,
					"estimated-minutes": this.tw_get_estimated_mins(),
					tags: tags,
				},
			};
		} else if (type == "toRedmine") {
			return {
				issue: {
					estimated_hours:
						this.rd_get_estimated_hours() == 0
							? undefined
							: this.rd_get_estimated_hours(),
					priority_id: this.rd_get_priority(this.core.priority),
					assigned_to_id: this.rd_get_assigned(
						this.core["responsible-party-id"]
					),
					custom_fields: [
						{
							id: Configuration.task_foreign_key_id,
							name: "ID_TASK_TW",
							value: this.core.id,
						},
					],
				},
			};
		} else if (type == "toRedmineResetID") {
			return {
				issue: {
					estimated_hours: this.rd_get_estimated_hours(),
					priority_id: this.rd_get_priority(this.core.priority),
					assigned_to_id: this.rd_get_assigned(
						this.core["responsible-party-id"]
					),
					custom_fields: [
						{
							id: Configuration.task_foreign_key_id,
							name: "ID_TASK_TW",
							value: "",
						},
					],
				},
			};
		}
	}
}
