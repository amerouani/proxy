export default class Configuration {
	static redirect_uri = import.meta.env.VITE_APP_REDIRECT_URI;
	static redmine_api_url = import.meta.env.VITE_APP_REDMINE_API_URL;
	static teamwork_api_url = import.meta.env.VITE_APP_TEAMWORK_API_URL;
	static user_foreign_key = "ID_USER_TW";
	static user_foreign_key_id = 10;
	static task_foreign_key = "ID_TASK_TW";
	static task_foreign_key_id = 11;
	static project_foreign_key = "ID_PROJECT_TW";
	static project_default_tasklist = "ID_TASKLIST_DEFAUT_TW";
	static project_foreign_key_ID = 12;
	static version_regex = "((?:[0-9]+.)*[0-9]+)";
	static teamwork_tags = [
		{
			id: 109685,
			name: "REDMINE",
			color: "#2f8de4",
			projectId: 0,
		},
		{
			id: 99366,
			name: "Intervention à distance [IN]",
			color: "#f78234",
			projectId: 0,
		},
		{
			id: 99365,
			name: "Intervention sur site [IN]",
			color: "#53c944",
			projectId: 0,
		},
		{
			id: 114486,
			name: "[CA]-CREDIT ASSISTANCE BE",
			color: "#2f8de4",
			projectId: 0,
		},
		{
			id: 114487,
			name: "[CA]-CREDIT ASSISTANCE DOC",
			color: "#2f8de4",
			projectId: 0,
		},
		{
			id: 114483,
			name: "[CA]-CREDIT ASSISTANCE MKG",
			color: "#2f8de4",
			projectId: 0,
		},
		{
			id: 114485,
			name: "[CA]-CREDIT ASSISTANCE PROD",
			color: "#2f8de4",
			projectId: 0,
		},
		{
			id: 114484,
			name: "[CA]-CREDIT ASSISTANCE SI",
			color: "#2f8de4",
			projectId: 0,
		},
	];
	static isDev = import.meta.env.NODE_ENV === "development";
}
